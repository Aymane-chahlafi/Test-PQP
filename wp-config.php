<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link https://fr.wordpress.org/support/article/editing-wp-config-php/ Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'wordpress' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '#6SKIGcA0VSLaZGF:!(~]((~-A05fTTcEQ$4qo<aUpc:+2rZ4NO)MCj!Q;KMOZad' );
define( 'SECURE_AUTH_KEY',  '&m]&oh2x]x*<zmS:&@pIUP}Um](l/5/+@T*)KRapoE6%MJg;yDK`.INSyU0lLE[,' );
define( 'LOGGED_IN_KEY',    '9}?7H7yD-,aay0 <{6{!PvjIM~-N^6ANAi k0j*t6N~iZQ>n:&Zu64u=djM|V)F]' );
define( 'NONCE_KEY',        '=9 U{#FZG_5<MM?q|lrZWzl:LY#VL0;]r|tb_52hQ_*&6^8L6;dPVdqXptOP<hM3' );
define( 'AUTH_SALT',        '}9t,,L r^QnaTSS(]5i^H0?M8*`[ncRyzML~Q%rVY=gnocBg~v|G|Iwu0JXGR2Z[' );
define( 'SECURE_AUTH_SALT', ')WjDNuac#N2R|J.g)1+4 b|oTLt[3krF!{mS(>UYJr?;tDuWDS3A<f!4*{u->$_Z' );
define( 'LOGGED_IN_SALT',   '.)66L[VJCnIetq:)}7!yfQoth3:I!aV^)t)vx-dc2/+*1)gmI7/V;Xo>o/OEv%:s' );
define( 'NONCE_SALT',       '>L(Vg,&%Nu8#z l&mSE,+_Y@7mfI,jGr.Qe3#{->xP+&S(M* 9x}R[R%gj>[zmVV' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs et développeuses : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs et développeuses d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur la documentation.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
