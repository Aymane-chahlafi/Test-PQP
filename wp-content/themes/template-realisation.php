<?php
/**
 * Template Name: Réalisations Template
 * Template Post Type: page
 */

get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">

        <?php
        // Display the custom post type "Réalisation" and pagination
        display_realisations();
        ?>

    </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
