<?php
    get_header();
    // The Query of post type {solution}.
    // $posts = new WP_Query( ['post_type'=>'realisation'] );
?>
<main class="main-solution">
    <h1 class="page-title">Nos réalisation</h1>
    
    <section class="nos_solutions" >
    <?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>
            <div class="solution" >
                <?php if (has_post_thumbnail()) : ?>
                    <div class="thumbnail">
                        <?php the_post_thumbnail('full'); ?>
                    </div>
                <?php endif; ?>
                <header class="entry-header" style="margin:1.5rem 0">
                    <a href="<?= get_permalink(); ?>" class="btn"><h2 class="entry-title"><?= the_title() ?></h2></a>
                </header>
                <div class="content">
                    <p><?= the_excerpt(); ?></p>
                </div>
                <!-- Button redirect to the solution post -->
                <a href="<?= get_permalink(); ?>" class="btn"><?= _e('Savoir plus'); ?></a>
            </div>
			<?php endwhile; ?>
		<?php endif; ?>

    </section>
    <?php
		// Previous/next page navigation.
		the_posts_pagination(
			array(
				'prev_text' => __( 'Previous', 'textdomain' ),
				'next_text' => __( 'Next', 'textdomain' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'textdomain' ) . ' </span>',
			)
		);
	?>

</main>
<?php
    wp_reset_postdata();
    get_footer();
?>