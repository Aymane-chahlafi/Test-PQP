<?php
/*
Plugin Name: Plugin Réalisation
Description: Gère les réalisations avec un custom post type et une pagination.
Version: 1.0
Author: Aymane
*/

// Exit si ce fichier est accédé directement.
if (!defined('ABSPATH')) {
    exit;
}

//Appeler les fichiers dans includes
require_once plugin_dir_path(__FILE__) . '/inc/custom-post-type.php';
require_once plugin_dir_path(__FILE__) . '/inc/settings-page.php';