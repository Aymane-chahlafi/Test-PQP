<?php
function realisation_plugin_menu() {
    add_options_page(
        'Configuration Réalisations',
        'Réalisations',
        'manage_options',
        'realisation-plugin-settings',
        'realisation_plugin_settings_page'
    );
}
add_action('admin_menu', 'realisation_plugin_menu');

function realisation_plugin_settings_page() {
    if (!current_user_can('manage_options')) {
        wp_die('Vous n\'avez pas l\'autorisation d\'accéder à cette page.');
    }

    $per_page = get_option('realisation_per_page', 10);

    if (isset($_POST['submit'])) {
        $per_page = absint($_POST['realisation_per_page']);
        update_option('realisation_per_page', $per_page);
        echo '<div class="notice notice-success is-dismissible"><p>Paramètres sauvegardés avec succès.</p></div>';
    }
    ?>
    <div class="wrap">
        <h1>Configuration des Réalisations</h1>
        <form method="post">
            <?php
            settings_fields('realisation_plugin_settings');
            do_settings_sections('realisation_plugin_settings');
            ?>
            <label for="realisation_per_page">Nombre de réalisations par page :</label>
            <input type="number" id="realisation_per_page" name="realisation_per_page" value="<?php echo esc_attr($per_page); ?>" min="1">
            <?php submit_button(); ?>
        </form>
    </div>
    <?php
}
