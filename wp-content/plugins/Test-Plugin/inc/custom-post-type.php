<?php
function create_realisation_custom_post_type() {
    $labels = array(
        'name' => 'Réalisations',
        'singular_name' => 'Réalisation',
        'menu_name' => 'Réalisations',
        'add_new' => 'Ajouter une réalisation',
        'add_new_item' => 'Ajouter une nouvelle réalisation',
        'edit_item' => 'Modifier la réalisation',
        'new_item' => 'Nouvelle réalisation',
        'view_item' => 'Voir la réalisation',
        'search_items' => 'Rechercher parmi les réalisations',
        'not_found' => 'Aucune réalisation trouvée',
        'not_found_in_trash' => 'Aucune réalisation trouvée dans la corbeille',
    );

    $args = [
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => "nos-realisations" ), // Le slug pour l'URL
        'capability_type'    => 'post',
        'has_archive'        => true ,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt', 'custom-fields' ), // Champs supportés dans l'éditeur de l'administration
    ];

    register_post_type('realisation', $args);
    flush_rewrite_rules();
        // return $this;
}
add_action('init', 'create_realisation_custom_post_type');
